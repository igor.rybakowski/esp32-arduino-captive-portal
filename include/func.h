#include "Arduino.h"
#include <EEPROM.h>


int which_ch(String hd);
bool off_on(String hd);
void operation(String command, uint &val, int &mode);
void read_eeprom(uint val1, bool val2);
void write_eeprom(uint val1, bool val2);
void write_channel(int channel, int val, bool tmp);

/* N-MOSFET FUNCTIONS */
void set_channel(int id, int pwm, bool enabled);