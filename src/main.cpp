/*
  Author: Igor Rybakowski

  To do:
    - save output values in EEPROM (done)
    - fix bug on channel1
    - delete passord
*/

#include <WiFi.h>
#include <DNSServer.h>
#include "func.h"
#include <EEPROM.h>
#include "images.h"


#define RESOLUTION_BITS 8
#define FREQUENCY 1000


int gpios[4] = {23,18,33,5};

const byte DNS_PORT = 53;
IPAddress apIP(192, 168, 1, 1);
DNSServer dnsServer;
WiFiServer server(80);

#define EEPROM_SIZE ((5*1)+(8*5))

// Variable to store the HTTP request
String header;
String command;
bool onoff = false;

bool channel = 0;
int time_mode = 2;
int power_mode = 0;

uint PWM = 0;
uint channel_pwm = 0;

//*********SSID and Pass for AP**************//
const char* ssidAPConfig = "jay23";
const char* passAPConfig = "jay23";

void display_html(WiFiClient clt);

void setup() {
  Serial.begin(115200);
  /* EEPROM init and read */
  if (!EEPROM.begin(EEPROM_SIZE)) {
    Serial.println("Failed to initialise EEPROM");
    Serial.println("Restarting...");
    delay(1000);
    ESP.restart();
  } 
  Serial.println("EEPROM init success");
  read_eeprom(channel_pwm, channel);

  /* PWM init */
  for(int i=0;i<4;i++) {
    pinMode(gpios[i], OUTPUT);
    digitalWrite(gpios[i], HIGH);

  }
  ledcSetup(0,FREQUENCY,RESOLUTION_BITS);
  for(int i=0;i<4;i++) {
    ledcAttachPin(gpios[i],0);
  }
  

  /* SETTING AP */
  Serial.print("Setting AP (Access Point)");
  WiFi.mode(WIFI_AP);
  WiFi.softAP(ssidAPConfig,passAPConfig);
  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
  dnsServer.start(DNS_PORT, "*", apIP);
  server.begin();
}

void loop() {
  WiFiClient client = server.available();   // listen for incoming clients
  dnsServer.processNextRequest();
  if (client) {
    Serial.println("New Client.");
    String currentLine = "";
    String temp = "";
    int what = 0;
    //while (client.connected()) {
      while (client.available()) {
        delay(1);
        char c = client.read();
        header += c;
        if (c == '\n') {
          if (currentLine.length() == 0) {
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println();
            delay(1);
            Serial.println(header);
            String sss = header.substring(23,24);
            Serial.println("[##### DEBUG MESSAGE #####]  header23=" + sss);
            if(sss != "l") {
              command = header.substring(0,20);
              Serial.println("[##### DEBUG MESSAGE #####] command : " + command);
      
              if(off_on(command) == true) {
                  channel = true;
                  Serial.println("Channel " + String(what) + " on");
              }
              else {
                  channel = false;
                  Serial.println("Channel " + String(what) + " off");
              }
              operation(command, channel_pwm, time_mode);
            }
            if(channel_pwm < 104)
              power_mode = 1;
            if(channel_pwm > 200)
              power_mode = 2;
            if(channel_pwm > 210)
              power_mode = 3; 
            display_html(client);
            break;
          } else {
            currentLine = "";
          }
        } else if (c != '\r') {
          currentLine += c;
        }
      }
    //}
    
    header = "";
    delay(1);
    Serial.println("Channel is" + String(channel));
    Serial.println("PWM: " + String(channel_pwm));
    client.stop();
    write_eeprom(channel_pwm, channel);

    Serial.println("Setting up PWM");
    write_channel(0, channel_pwm, channel);

    Serial.println("Client disconnected\r\n");
  }
  delay(10);
}


void display_html(WiFiClient clt) {
  clt.println("<!DOCTYPE html><html>");
  clt.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
  clt.println("<link rel=\"icon\" href=\"data:,\">");
  clt.println("<style>body {background-color : #f2f1ec;} html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
  clt.println(".button {opacity: 0.5; display: inline-block; background-color: #f2f1ec; border: none; color: black; padding: 30px 10px; text-align: center;");
  clt.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
  clt.println(".button3 {background-color #f2f1ec; border: none; padding: 0px 30px;");
  clt.println("text-decoration: none; font-size: 10px; margin: 2px; cursor: pointer;}");
  clt.println(heating);
  clt.println(duration);
  clt.println(mockup);
  clt.println(arrow);
  clt.println(".active {opacity: 1;}");
  clt.println("</style></head>");

  clt.println("<body><p><div class=\"mockup\"></div></p>");
  clt.println("<p> &nbsp;</p>");
  clt.println("<p> &nbsp;</p>");
  clt.println("<p><div class=\"heating\"></div></p>");
  
  // CHANNEL 0
  clt.println("<p>Channel - State " + String(channel) + " Power : " + String(power_mode) + "</p>");     
  if (channel==false) {
    clt.print("<p><a href=\"/9/on\"><button class=\"button\">ON</button></a>");
    if(power_mode == 1) {
      clt.println("<a href=\"/9/of/1\"><button class=\"button button3\"> <div class=\"arrow active\"> </div></button></a>");
      clt.println("<a href=\"/9/of/2\"><button class=\"button button3\"> <div class=\"arrow\"> </div> <div class=\"arrow\"> </div></button></a>");
      clt.println("<a href=\"/9/of/3\"><button class=\"button button3\"> <div class=\"arrow\"> </div> <div class=\"arrow \"> </div> <div class=\"arrow \"> </div></button></a>");
    }
    if(power_mode == 2)  {
      clt.println("<a href=\"/9/of/1\"><button class=\"button button3\"> <div class=\"arrow\"> </div></button></a>");
      clt.println("<a href=\"/9/of/2\"><button class=\"button button3\"> <div class=\"arrow active\"> </div> <div class=\"arrow active\"> </div></button></a>");
      clt.println("<a href=\"/9/of/3\"><button class=\"button button3\"> <div class=\"arrow\"> </div> <div class=\"arrow \"> </div> <div class=\"arrow \"> </div></button></a>");
    }
    if(power_mode == 3)  {
      clt.println("<a href=\"/9/of/1\"><button class=\"button button3\"> <div class=\"arrow\"> </div></button></a>");
      clt.println("<a href=\"/9/of/2\"><button class=\"button button3\"> <div class=\"arrow\"> </div> <div class=\"arrow\"> </div></button></a>");
      clt.println("<a href=\"/9/of/3\"><button class=\"button button3\"> <div class=\"arrow active\"> </div> <div class=\"arrow active\"> </div> <div class=\"arrow active\"> </div></button></a>");
    }
  } else {
    clt.print("<p><a href=\"/9/of\"><button class=\"button \">OFF</button></a>");
    if(power_mode==1) {
      clt.println("<a href=\"/9/on/1\"><button class=\"button button3\"> <div class=\"arrow active\"> </div></button></a>");
      clt.println("<a href=\"/9/on/2\"><button class=\"button button3\"> <div class=\"arrow\"> </div> <div class=\"arrow\"> </div></button></a>");
      clt.println("<a href=\"/9/on/3\"><button class=\"button button3\"> <div class=\"arrow\"> </div> <div class=\"arrow\"> </div> <div class=\"arrow\"> </div></button></a>");
    }
    if(power_mode==2) {
      clt.println("<a href=\"/9/on/1\"><button class=\"button button3\"> <div class=\"arrow\"> </div></button></a>");
      clt.println("<a href=\"/9/on/2\"><button class=\"button button3\"> <div class=\"arrow active\"> </div> <div class=\"arrow active\"> </div></button></a>");
      clt.println("<a href=\"/9/on/3\"><button class=\"button button3\"> <div class=\"arrow\"> </div> <div class=\"arrow\"> </div> <div class=\"arrow\"> </div></button></a>");
    }
    if(power_mode==3) {
      clt.println("<a href=\"/9/on/1\"><button class=\"button button3\"> <div class=\"arrow\"> </div></button></a>");
      clt.println("<a href=\"/9/on/2\"><button class=\"button button3\"> <div class=\"arrow\"> </div> <div class=\"arrow\"> </div></button></a>");
      clt.println("<a href=\"/9/on/3\"><button class=\"button button3\"> <div class=\"arrow active\"> </div> <div class=\"arrow active\"> </div> <div class=\"arrow active\"> </div></button></a>");
    }
    
  }
  //clt.println("<p><div class=\"clock\"></div></p>");
  clt.println("<p> &nbsp;</p>");
  clt.println("<p><div class=\"duration\"></div></p>");
  if(channel==false) {
    if(time_mode==1) 
      clt.print("<p><a href=\"/9/of/4/\"><button class=\"button active\">10 min </button></a><a href=\"/9/of/5\"><button class=\"button\">60 min </button></a><a href=\"/9/of/6\"><button class=\"button\">120 min</button></a></p>");
    else if(time_mode==2)
      clt.print("<p><a href=\"/9/of/4/\"><button class=\"button\">10 min </button></a><a href=\"/9/of/5\"><button class=\"button active\">60 min </button></a><a href=\"/9/of/6\"><button class=\"button\">120 min</button></a></p>");
    else if(time_mode==3)
      clt.print("<p><a href=\"/9/of/4/\"><button class=\"button\">10 min </button></a><a href=\"/9/of/5\"><button class=\"button\">60 min </button></a><a href=\"/9/of/6\"><button class=\"button active\">120 min</button></a></p>");
  }
  else {
    if(time_mode==1) 
      clt.print("<p><a href=\"/9/on/4/\"><button class=\"button active\">10 min </button></a><a href=\"/9/on/5\"><button class=\"button\">60 min </button></a><a href=\"/9/on/6\"><button class=\"button\">120 min</button></a></p>");
    else if(time_mode==2)
      clt.print("<p><a href=\"/9/on/4/\"><button class=\"button\">10 min </button></a><a href=\"/9/on/5\"><button class=\"button active\">60 min </button></a><a href=\"/9/on/6\"><button class=\"button\">120 min</button></a></p>");
    else if(time_mode==3)
      clt.print("<p><a href=\"/9/on/4/\"><button class=\"button\">10 min </button></a><a href=\"/9/on/5\"><button class=\"button\">60 min </button></a><a href=\"/9/on/6\"><button class=\"button active\">120 min</button></a></p>");
  }
  

  clt.println("</body></html>");
  // The HTTP response ends with another blank line
  clt.println();
}
