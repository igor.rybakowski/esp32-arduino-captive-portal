#include "func.h"
#include <EEPROM.h>

void read_eeprom(uint val1, bool val2) {
    int address = 0;
    val2 = EEPROM.readBool(address);
    Serial.println("CHANNEL IS :" + val2);
    address+= sizeof(bool);
    val1 = EEPROM.readShort(address);
    Serial.println("UINT READ : " +String(val1));
    address+=sizeof(uint);
    Serial.println("Loaded data from EEPROM");
}
void write_eeprom(uint val1, bool val2) {
    int address = 0;
    EEPROM.writeBool(address, val2);
    address+= sizeof(bool);
    EEPROM.writeShort(address, val1);
    address+=sizeof(uint);
    EEPROM.commit();
    Serial.println("Saved data to EEPROM");
}

bool off_on(String hd) {
  String temp = "";
  temp = hd.substring(8,9);
  Serial.println("[##### DEBUG MESSAGE #####] OFF ON SUBSTRING: " + temp);
  if(temp[0] == 'n') {
    return true;
  }
  return false;
}

void operation(String cmd, uint &val, int &mode) {
  Serial.println("[##### DEBUG MESSAGE #####] CMD at 10 is : " + cmd[10]);
  if(cmd[10] == '1'){
    val = 102;
  }
  else if(cmd[10] == '2') {
    val = 204;
  }
  else if(cmd[10] == '3') {
    val = 255;
  }
  else if(cmd[10] == '4') {
    mode = 1;
  }
  else if(cmd[10] == '5') {
    mode = 2;
  }
  else if(cmd[10] == '6') {
    mode = 3;
  }
}

void write_channel(int channel, int val, bool tmp) {
    if(tmp == true)
      ledcWrite(channel, val);
    else
      ledcWrite(channel,0);
}
